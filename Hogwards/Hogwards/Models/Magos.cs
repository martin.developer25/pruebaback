﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hogwards.Models
{
    public class Magos
    {
        [Key]
        [Required]
        public int id { get; set; }
        
        [Required]
        [StringLength(20)]
        public string nombre { get; set; }
        
        [Required]
        [StringLength(100)]

        public string apellido { get; set; }
        
        [Required]
        [Range(0, 9999999999)]
        public int identificador { get; set; }
        
        [Required]
        [Range(0, 99)]
        public int edad { get; set; }
        
        [Required]
        [StringLength(100)]

        public string casa { get; set; }
    }
}
