﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hogwards.Models
{
    public class Casas
    {
        [Key]
        [Required]
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string nombre { get; set; }
    }
}
