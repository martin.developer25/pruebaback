﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hogwards.Context;
using Hogwards.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Hogwards.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MagosController : ControllerBase
    {

        private readonly AppDbContext dbContext;

        public MagosController(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Muestra a todos los estudiantes de Hogwards
        /// </summary>
        /// <returns></returns>
        public ActionResult<IEnumerable<Magos>> Get()
        {
            return Ok(dbContext.magos.ToList());
        }

        /// <summary>
        /// Crea nuevos estudiantes, y valida la casa
        /// </summary>
        /// <param name="estudiante"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post([FromBody] Magos estudiante)
        {
            if (!ModelState.IsValid)
                return BadRequest("La informacion magica no es valida");

            var dbMagic = dbContext.casas
                .Where(c => c.nombre == estudiante.casa)
                .FirstOrDefault();

            if (dbMagic == null)
                return BadRequest($"Esta casa {estudiante.casa} no pertenece a Hogwards");
            
            dbContext.magos.Add(estudiante);
            dbContext.SaveChanges();
            
            return Ok("Bienvenido al colegio de Magia y Hechizeria");
        }

        /// <summary>
        /// Permite modificar los registro del estudiante, por el identificador
        /// </summary>
        /// <param name="estudiante"></param>
        /// <returns></returns>
        [HttpPut]
        public ActionResult Put([FromBody] Magos estudiante)
        {
            if (!ModelState.IsValid)
                return BadRequest("La informacion magica no es valida");

            var dbMagic = dbContext.magos
                .Where(m => m.identificador == estudiante.identificador)
                .FirstOrDefault();

            if (dbMagic == null)
                return BadRequest($"El muggle {estudiante.identificador} no se encuentra");

            dbMagic.nombre = estudiante.nombre;
            dbMagic.apellido = estudiante.apellido;
            dbMagic.identificador = estudiante.identificador;
            dbMagic.edad = estudiante.edad;
            dbMagic.casa = estudiante.casa;

            dbContext.Update(dbMagic);
            dbContext.SaveChanges();

            return Ok();
        }

        /// <summary>
        /// Permite lanzar una avada kedabra a un estudiante
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Route("{id}")]
        public ActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest("La informacion magica no es valida");
            var dbMagic = dbContext.magos
               .Where(m => m.identificador == id)
               .FirstOrDefault();

            if (dbMagic == null)
                return BadRequest($"El muggle {id} no se encuentra");

            dbContext.Remove(dbMagic);
            dbContext.SaveChanges();
            return Ok();

        }
    }
}
